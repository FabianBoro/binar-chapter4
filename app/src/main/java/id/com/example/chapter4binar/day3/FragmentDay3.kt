package id.com.example.chapter4binar.day3

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.com.example.chapter4binar.R
import id.com.example.chapter4binar.adapter.ContactAdapter
import id.com.example.chapter4binar.databinding.FragmentDay3Binding


class FragmentDay3 : Fragment() {
    private var _binding: FragmentDay3Binding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentDay3Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initRecyclerView() {
//        val adapterContact = ContactAdapter(daftarContacts)

    }

}