package id.com.example.chapter4binar.datamodel

data class DataPerson(
    val nama: String,
    val alamat: String
)
