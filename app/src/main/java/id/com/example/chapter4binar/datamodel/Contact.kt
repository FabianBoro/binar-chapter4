package id.com.example.chapter4binar.datamodel

data class Contact(
    val nama: String,
    val phone: Int
)
