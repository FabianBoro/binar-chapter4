package id.com.example.chapter4binar.adapter

import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import id.com.example.chapter4binar.R
import id.com.example.chapter4binar.datamodel.Contact


class ContactAdapter(val daftarContact: ArrayList<Contact>)
    : RecyclerView.Adapter<ContactAdapter.ContactViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        val it = LayoutInflater.from(parent.context).inflate(R.layout.item_contact, parent, false)
        return ContactViewHolder(it)
    }

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        holder.bind(daftarContact[position])
    }

    override fun getItemCount(): Int = daftarContact.size

    inner class ContactViewHolder(it: View): RecyclerView.ViewHolder(it){
        val tvNama: TextView = it.findViewById(R.id.tv_nama)
        val tvPhone: TextView = it.findViewById(R.id.tv_phone)

        fun bind(item: Contact){
            tvNama.text = item.nama
            tvPhone.text = item.phone.toString()
        }
    }
}
