package id.com.example.chapter4binar.day1

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import id.com.example.chapter4binar.R
import id.com.example.chapter4binar.databinding.FragmentDay1Binding



class FragmentDay1 : Fragment() {
    private var _binding: FragmentDay1Binding? = null
    private val binding get() = _binding!!
    private lateinit var snackbar2: Snackbar

//    var editTextName =

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentDay1Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

//    StartCode
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showToast()
        snackbar2 = Snackbar.make(binding.btnSnackBar, "tombol snackbar telah ditekan", Snackbar.LENGTH_LONG)
        tugasSnackbar()
    }

    private fun showSnackbar(){
        binding.btnSnackBar.setOnClickListener {
            snackbar2 = Snackbar.make(it, "Button telah ditekan", Snackbar.LENGTH_LONG)
            snackbar2.show()
        }
    }

    private fun showSnackbarWithAction(){
        binding.btnSnackBarAction.setOnClickListener {

        }
    }

    private fun showToast() {
        binding.btnToast.setOnClickListener {
            createToast("Button Toast telah di tekan!").show()
        }
    }


    private fun showToastNama(){
        binding.submitNama.setOnClickListener {
            val valueEditText = it.findViewById<EditText>(R.id.nama)
            createToast("$valueEditText").show()
//            createToast($valueEditText)
        }
    }

    private fun showSnackBarWithAction() {
        binding.btnSnackBarAction.setOnClickListener {
            customColorSnackBar(it, "Button SnackBar dengan Action di tekan!")
                .setAction(getString(R.string.txt_take_action)) {
                    createToast("Take action di tekan!").show()
                }
                .show()
        }
    }


    private fun createToast(message: String): Toast {
        return Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT)
    }

    private fun createSnackBar(it: View, message: String): Snackbar {
        return Snackbar.make(it, message, Snackbar.LENGTH_INDEFINITE)
    }

    private fun customColorSnackBar(it: View, message: String) : Snackbar {
        val snackbar = createSnackBar(it, message)
        snackbar.setBackgroundTint(ContextCompat.getColor(requireContext(), R.color.purple_500))
            .setActionTextColor(ContextCompat.getColor(requireContext(), R.color.black))
            .setTextColor(ContextCompat.getColor(requireContext(), R.color.teal_200))
        return snackbar
    }

//    fun Snackbar.addAction(@LayoutRes aLayoutId: Int, @StringRes aLabel: Int, aListener: View.OnClickListener?) : Snackbar {
//        addAction(aLayoutId,context.getString(aLabel),aListener)
//        return this;
//    }

    private fun tugasSnackbar(){
        binding.menampilkanDuaActionSnackBar.setOnClickListener{
            customColorSnackBar(it, "aksi nya 1 aja ya hehe")
        }
    }


}