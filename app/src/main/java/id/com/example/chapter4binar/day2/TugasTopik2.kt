package id.com.example.chapter4binar.day2

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.com.example.chapter4binar.R
import id.com.example.chapter4binar.databinding.FragmentTugasTopik2Binding


class TugasTopik2 : Fragment() {
    private var _binding: FragmentTugasTopik2Binding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentTugasTopik2Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    private fun alertDialogStandard(){
        binding.alertDialogStandard.setOnClickListener {
            val dialog = AlertDialog.Builder(requireContext())
            dialog.setTitle("ALERT. Tidur lah pls!!")
            dialog.setMessage("Kamu kurang tidur bang, tidur aja. yok yok skala prioritas " +
                    "Kerjaan ditinggal dulu aja, kesehatan lebih penting bang!!")
//            dialog.setCancelable(false)
            dialog.show()
        }
    }

    private fun alertDialogWithAksiBtn(){
        binding.alertDialogWithActionBtn.setOnClickListener {
            val dialog = AlertDialog.Builder(requireContext())
            dialog.setTitle("Title nih")
            dialog.setMessage("Kamu kurang tidur bang, tidur aja. yok yok skala prioritas \" +\n" +
                    "                    \"Kerjaan ditinggal dulu aja, kesehatan lebih penting bang!!")
            dialog.setPositiveButton("Tidur") { dialogInterface, angka ->

            }
            dialog.setNeutralButton("Ignore") { dialogInterface, _ ->

            }
            dialog.setNegativeButton("Ga Tidur") { dialogInterface, _ ->

            }
            dialog.setCancelable(false)
            dialog.show()
        }

    }

    private fun customDialog(){

    }

}